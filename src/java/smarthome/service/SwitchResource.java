package smarthome.service;

import smarthome.device.*;
import javax.ejb.*;
import javax.ws.rs.*;

@Path("switch")
public class SwitchResource {
    @EJB
    private SwitchBean switchBean;
   
    @GET
    public Device peek() {
        return switchBean.peek();
    }

    @POST
    public Device turn(boolean device) {
        return switchBean.turn(device);
    }
}
