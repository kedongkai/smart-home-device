package smarthome.device;

import java.util.*;
import javax.annotation.*;
import javax.ejb.*;
import javax.inject.Named;

@Named
@Singleton
public class SwitchBean {
    @Resource(name = "deviceName")
    String deviceName;
    @Resource(name = "deviceId")
    int deviceId;
    
    private Device device;

    public Device getDevice() {
        return device;
    }

    @PostConstruct
    protected void init() {
        device = new Device();
        device.setId(deviceId);
        device.setName(deviceName);
        device.setUpdateTime(new Date());
        device.setStatus(false);
    }
    
    public Device turn(boolean status) {
        device.setStatus(status);
        device.setUpdateTime(new Date());
        return device;
    }
    
    public Device peek() {
        return device;
    }
    
    public void reload() {
        
    }
}
