$(document).ready(
    function() {
        setInterval(function() {
            $('#autoRefreshForm\\:btnLoadData').click();
            updateContent();
            //$('#result').reload();
        }, 1000);
    }
);

$(function() {
    updateContent();
});

function updateContent() {
    var status = $("#status").text();
    if (status === "true") {
        $("#img-on").show();
        $("#img-off").hide();
        $("#on-or-off").text("On");
    }
    else {
        $("#img-off").show();
        $("#img-on").hide();
        $("#on-or-off").text("Off");
    }
}